# frozen_string_literal: true

require Rails.root.join("spec/support/helpers/stub_requests.rb")

Dir[Rails.root.join("jh/spec/support/helpers/*.rb")].sort.each { |f| require f }
Dir[Rails.root.join("jh/spec/support/shared_contexts/*.rb")].sort.each { |f| require f }
Dir[Rails.root.join("jh/spec/support/shared_examples/*.rb")].sort.each { |f| require f }
Dir[Rails.root.join("jh/spec/support/**/*.rb")].sort.each { |f| require f }
