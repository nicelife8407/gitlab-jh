# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ::Gitlab::Database do
  describe '.config' do
    it 'adds "jh/db/migrate" to application config' do
      expect(Rails.application.config.paths['db/migrate']).to include described_class::JH_MIGRATE_PATH
    end
  end
end
