# frozen_string_literal: true

module JH
  module Gitlab
    module SubscriptionPortal
      extend ActiveSupport::Concern

      class_methods do
        extend ::Gitlab::Utils::Override

        override :default_subscriptions_url
        def default_subscriptions_url
          ::Gitlab.dev_or_test_env? ? 'https://customers.stg.gitlab.cn' : 'https://customers.gitlab.cn'
        end
      end
    end
  end
end
