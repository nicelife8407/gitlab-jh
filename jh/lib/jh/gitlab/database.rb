# frozen_string_literal: true

module JH
  module Gitlab
    module Database
      extend ActiveSupport::Concern

      JH_MIGRATE_PATH = 'jh/db/migrate'

      class_methods do
        extend ::Gitlab::Utils::Override

        override :config
        def config
          db_migrate_paths = Rails.application.config.paths['db/migrate']
          unless db_migrate_paths.include?(JH_MIGRATE_PATH)
            db_migrate_paths << JH_MIGRATE_PATH
          end

          super
        end
      end
    end
  end
end
