# frozen_string_literal: true

module JH
  module BillingPlansHelper
    extend ::Gitlab::Utils::Override

    override :use_new_purchase_flow?
    def use_new_purchase_flow?(namespace)
      false
    end
  end
end
